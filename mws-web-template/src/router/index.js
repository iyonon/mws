import Vue from 'vue'
import Router from 'vue-router'
import PrintPage from '@/components/PrintPage'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Print',
      component: PrintPage
    }
  ]
})
