package com.yonon.mws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MWSApplication {
    public static void main(String[] args) {
        SpringApplication.run(MWSApplication.class, args);
    }
}
