package com.yonon.mws.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {
    private static Logger logger = LogManager.getLogger(MainController.class);

    @GetMapping(value = "/print")
    public String print(@RequestParam("name") String name) {
        logger.info("vue page print msg—— input name:{}", name);
        logger.debug("vue page print msg—— input name:{}", name);
        System.out.println("vue page print msg " + name);
        return name;
    }

    @GetMapping(value = "/index")
    public String print() {
        logger.info("vue page print msg no param");
        return "you can input param with : http://localhost:8080/print?name='HelloWorld'";
    }
}
